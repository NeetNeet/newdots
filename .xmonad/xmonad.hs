--  _   _                __  __                                _ 
-- | \ | | _____      __ \ \/ /_ __ ___   ___  _ __   __ _  __| |
-- |  \| |/ _ \ \ /\ / /  \  /| '_ ` _ \ / _ \| '_ \ / _` |/ _` |
-- | |\  |  __/\ V  V /   /  \| | | | | | (_) | | | | (_| | (_| |
-- |_| \_|\___| \_/\_/   /_/\_\_| |_| |_|\___/|_| |_|\__,_|\__,_|
                                                              


-- Main Imports
import XMonad
import Data.Monoid
import System.Exit
import System.IO

-- Util Imports
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig (additionalKeysP, mkKeymap)

-- Hook Imports
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops

-- Layout Imports
import XMonad.Layout.Gaps
import XMonad.Layout.Spacing
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.WindowArranger
import XMonad.Layout.ShowWName
import XMonad.Layout.ThreeColumns

-- Action Imports
import XMonad.Actions.MouseResize

-- Other Imports (idfk what these do)
import qualified XMonad.StackSet as W
import qualified Data.Map as M

-- Setting Alt key as mod key
myModMask :: KeyMask
myModMask = mod1Mask

-- Programs
myTerminal :: String
myTerminal = "alacritty"

-- Setting focus to click instead of following mouse
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

myClickJustFocuses :: Bool
myClickJustFocuses = True

-- Border Settings
myBorderWidth = 8
myNormColor = "#dddddd"
myFocusColor = "#45d9ff"

-- Keybindings
myKeys = [
    ("M-s", spawn "/home/vivian/scripts/screenshot.sh")
    , ("<XF86AudioMute>", spawn "amixer set Master toggle")
    , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
    , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
    , ("<XF86AudioPlay>", spawn "mpc toggle")
    , ("<XF86AudioNext>", spawn "mpc next & /home/vivian/scripts/song-skipped.sh")
    , ("S-<XF86AudioNext>", spawn "mpc seek +00:00:02")
    , ("<XF86AudioPrev>", spawn "mpc prev")
    , ("S-<XF86AudioPrev>", spawn "mpc seek -00:00:02")
    , ("<XF86AudioStop>", spawn " mpc stop")
    , ("M-b", spawn "/home/vivian/scripts/dmenu-browser.sh")
    , ("M-S-n", spawn "alacritty -e newsboat")
    , ("M-S-m", spawn "alacritty -e ncmpcpp")
    , ("M-S-s", spawn "/home/vivian/scripts/now-playing.sh")
    ]

-- Start up shit
myStartupHook = do
    -- My monitor set up
    spawnOnce "/home/vivian/scripts/screens.sh &"
    -- My music stuff
    spawnOnce "alacritty -e ncmpcpp &"
    spawnOnce "mopidy &"
    -- Programs I wanna run at startup
    spawnOnce "signal-desktop &"
    spawnOnce "discord &"
    -- Run compositor, notification system and set my background
    spawnOnce "picom -b --experimental-backends &"
    spawnOnce "feh --bg-fill ~/Pictures/Wallpapers/NeetLinuxBG.jpg &"
    spawnOnce "xsetroot -cursor_name left_ptr &"
    spawnOnce "dunst"


-- Layouts
myLayout =
  smartBorders $
  spacingRaw True (Border 0 10 10 10) True (Border 10 10 10 10) True $
  avoidStruts $
  mouseResize $
  windowArrange $
  tiled
  ||| Mirror tiled
  ||| Full
  ||| layoutColumns
  ||| Mirror layoutColumns
  where
    -- default tiling algorithm partitions the screen into two panes 
    tiled   = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportio
    -- Move focus to the next window
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

    -- Columns test
    layoutColumns = ThreeCol nmaster delta ratio

-- Colors
pastelpink = "#fc88f0"
red        = "#fb4934"
blueish    = "#4efcf0"
deepblue   = "#2702f9"
purple     = "#8e4efc"

-- Log hook
myLogHook h = dynamicLogWithPP $ def 
    {ppOutput = io . appendFile "/tmp/.xmonad-workspace-log" . flip (++) "\n"
    , ppCurrent = wrap ("%{F" ++ blueish ++ "} ") " %{F-}"
    , ppVisible = wrap ("%{F" ++ deepblue ++ "} ") " %{F-}"
    , ppLayout = wrap ("%{F" ++ pastelpink ++ "} ") " %{F-}"
    , ppTitle = wrap ("%{F" ++ purple ++ "} ") " %{F-}"
    , ppSep = " | "
    }

-- Window Management
myManageHook = composeAll
    [ className =? "spotify" --> doShift ( myWorkspaces !! 2)
    , className =? "Signal"  --> doShift ( myWorkspaces !! 2)
    , className =? "discord" --> doShift ( myWorkspaces !! 2)
    ]

-- Workspaces
-- myWorkspaces = ["1","2","3","4","5","6","7","8","9"]
myWorkspaces = ["1:main","2:sec","3:game","4:comms","5:misc","6","7","8","9"]


-- Run everything
main = do
    xmproc <- spawnPipe "/home/vivian/.config/polybar/launch.sh"
    xmonad $ ewmh $ def {
      terminal = myTerminal
      , focusFollowsMouse = myFocusFollowsMouse
      , clickJustFocuses = myClickJustFocuses
      , borderWidth = myBorderWidth
      , normalBorderColor = myNormColor
      , focusedBorderColor = myFocusColor
      , modMask = myModMask
      , logHook = myLogHook xmproc
      , layoutHook = myLayout
      , manageHook = myManageHook <+> manageDocks
      , handleEventHook = docksEventHook
      , startupHook = myStartupHook
      } `additionalKeysP` myKeys
