### Neet's Xmonad Config

This is my xmonad config. It's pretty simple. I owe a huge thanks to AusCyber for sitting down and helping me set this up originally.<br>
Without them, this set up wouldn't of happened.<br>
<br>
**Font:** RobotoMono Nerd Font
**Layouts:** Tiled, Mirrored Tiled, Fullscreen (more to be added later)
**Important things:**<br>
- **Xmonad.Util.EZConfig** makes setting keybinds for xmonad so much easier. Much recommended.
- **Xmonad.Layout.Gaps** adds gaps between my windows giving it that slight i3-gaps feel.<br>
I still haven't figured out how to make my workspace names show up in polybar.
