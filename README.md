### Neet's Personal Dots
This is the shithole in where all my dots reside.

![My desktop](/assests/desktop.png)

**Distro:** Arch Linux<br>
**DE/WM:** Xmonad<br>
**Terminal:** Alacritty<br>
**Shell:** Fish<br>
**Editor:** Nvim

