#! /bin/bash
declare options=("Shutdown
Reboot
Logout
Cancel")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Please select an option: ' -l 3)
case "$choice" in
	Shutdown)
		choice="poweroff"
	;;
	Reboot)
		choice="reboot"
	;;
	Logout)
		choice="exit"
	;;
	Cancel)
		echo "Program terminated." && exit 1
	;;
esac
$choice
