### Neet's simple scripts
These are a collection of my scripts that I made to just do basic shit.<br>
[Screens.sh](https://gitlab.com/NeetNeet/newdots/-/blob/master/scripts/screens.sh) was made to set up my monitors. Nothing special, it was really made by arandr not me.<br>
[Screenshot.sh](https://gitlab.com/NeetNeet/newdots/-/blob/master/scripts/screenshot.sh) is just the screenshot command I use made into a script for easier use in configs and stuff.<br>
[Dots_Update.sh](https://gitlab.com/NeetNeet/newdots/-/blob/master/scripts/dots_update.sh) is my script I made back in my [old dots](https://gitlab.com/NeetNeet/dots) to make updating my dots much easier. Tbh rarely use it.<br>
[Dmenu-browser.sh](https://gitlab.com/NeetNeet/newdots/-/blob/master/scripts/dmenu-browser.sh) is just a small dmenu script made to lemme pick which one out of the two browsers I use.<br>
Power-menu.sh we don't talk about. That's still a work in progress/failure.
