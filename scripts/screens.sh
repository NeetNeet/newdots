#!/bin/sh
# ____                             ____       _               
#/ ___|  ___ _ __ ___  ___ _ __   / ___|  ___| |_ _   _ _ __  
#\___ \ / __| '__/ _ \/ _ \ '_ \  \___ \ / _ \ __| | | | '_ \ 
# ___) | (__| | |  __/  __/ | | |  ___) |  __/ |_| |_| | |_) |
#|____/ \___|_|  \___|\___|_| |_| |____/ \___|\__|\__,_| .__/ 
#                                                      |_|    

xrandr --output DP-0 --mode 1920x1080 --pos 3840x120 --rotate left --output DP-1 --off --output DP-2 --off --output DP-3 --off --output HDMI-0 --off --output DP-4 --primary --mode 3840x2160 --pos 0x0 --rotate normal --output DP-5 --off
