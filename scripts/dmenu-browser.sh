#! /bin/bash
declare options=("firefox
qutebrowser
cancel")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Please choose a browser: ' -l 3)

case "$choice" in
	firefox)
		choice="firefox"
	;;
	qutebrowser)
		choice="qutebrowser"
	;;
	cancel)
		echo "Program terminated." && exit 1
	;;
esac
$choice
