### Neet's .config folder

Here's where I keep my configs for mainly everything I use. There's nothing too exciting here.
- **Alacritty:** My preferred terminal. Just basic configs like changing font and setting fish as my default shell.
- **Fish:** My preferred shell, I used to use ZSH but I've since moved to Fish and I love the features it has out the box.
- **Newsboat:** I kinda use this rarely but it's definitely nice to have on the occasion I do want to use it.
- **Nvim:** My *much* preferred editor. I used to use emacs but since moved to nvim as I like the very low resources and minimalism of it.
- **Polybar:** Pretty self-explanitory, I use polybar to see what I'm actually doing, the time and some basic system information.
- **Qutebrowser:** This is one of my newer additions, I use it for basic browsing, quick web searches and casual browsing on things that don't need logins.
- **Xmobar:** I don't use this but I keep it as a backup incase I need it.
- **Picom:** This is a config a friend gave me with some small adjustments here and there for my liking.
