-- ==================================================
-- Initiliazization
-- ==================================================

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

-- Define Mod Keys
local modkey = "Mod4"
local altkey = "Mod1"

-- define module table
local keys = {}


-- =================================================
-- Movement keys
-- =================================================

keys.clientbuttons = gears.table.join(
-- Move focus
  awful.key({modkey,}, "j",
    function()
      awful.client.focus.byidx( 1)
    end,
    {description = "focus next by index", group = "movement"}
	),

  awful.key({modkey,}, "k",
    function()
      awful.client.focus.byidx(-1)
    end,
    {description = "focus previous by index", group = "movement"}
	),

-- Move window around
  awful.key({modkey, "Shift"}, "j",
    function()
      awful.client.swap.byidx( 1)
    end,
    {description = "swap with next client by index", group = "window"}
	),

  awful.key({modkey, "Shift"}, "k",
    function()
      awful.client.swap.byidx(-1)
    end,
    {description = "swap with previous client by index". group = "window"}
	),

-- Screens control
  awful.key({modkey, "Control"}, "j",
    function()
      awful.screen.focus_relative( 1)
    end,
    {description = "swap screens +1", group = "movement"}
	),

  awful.key({modkey, "Control"}, "k",
    function()
      awful.screen.focus_relative(-1)
    end,
    {description = "swap screens -1", group = "movement"}
	),

-- Jump to urgent tag
  awful.keys({modkey,}, "u",
    function()
      awful.client.urgent.jumpto,
    end,
    {description = "move to urgent window", group = "movement"}
  ),

)
-- ===================================================
-- Launcher
-- ===================================================

keys.clientbuttons = gears.table.join(
-- Spawn Terminal
  awful.key({modkey,}, "Return",
    function()
      awful.spawn(terminal)
    end,
    {description = "spawn a terminal", group = "launcher"}
  ),

  awful.key({modkey}, "p",
    function()
      menubar.show()
    end,
    {description = "show the menubar", group = "launcher"}
	),

  awful.key({modkey,}, "r",
    function()
      awful.screen.focused().mypromptbox:run()
    end,
    {description = "run prompt". group = "launcher"}
  ),
)

-- ===================================================
-- Layout management
-- ===================================================

keys.clientbuttons = gears.table.join(
-- Master Node Manipulation
  awful.key({modkey,}, "l",
    function()
      awful.tag.incmwfact( 0.05)
    end,
    {description = "increase master window size", group = "layout"}
	),

  awful.key({modkey,}, "h",
    function()
      awful.tag.incmwfact(-0.05)
    end,
    {description = "decrease master window size", group = "layout"}
	),

  awful.key({modkey, "Shift"}, "h",
    function()
      awful.tag.incnmaster(1, nil, true)
    end,
    {description = "increase number of master windows", group = "layout"}
	),

  awful.key({modkey, "Shift"}, "l",
    function()
      awful.tag.incnmaster(-1, nil, true)
    end,
    {description = "decrease number of master windows", group = "layout"}
	),
-- Changing Layouts
  awful.key({modkey,}, "space",
    function()
      awful.layout.inc( 1)
    end,
    {description = "select next layout", group = "layout"}
	),

  awful.key({modkey, "Shift"}, "space",
    function()
      awful.layout.inc(-1)
    end,
    {description = "select previous layout", group = "layout"}
	),
)

-- ====================================================
-- Function Keys
-- ====================================================

keys.clientbuttons = gears.table.join(
  -- Volume Keys
  awful.key({}, "XF86AudioRaideVolume",
    function()
      awful.spawn("amixer set Master 5%+ unmute", false)
      awsome.emit_signal("volume_change")
    end,
  ),

  awful.key({}, "XF86AudioLowerVolume",
    awful.spawn("amixer set Master 5%- unmute")
    awesome.emit_signal("volume_change")
    end,
  ),
)
::
-- ====================================================
-- Tags
-- ====================================================

-- TODO Make tags keybinds.
keys.clientbuttons = gears.table.join(
  -- Viewing tags
  awful.key({modkey}, "#" .. i + 9,
    function ()
      local screen = awful.screen.focused()
      local tag - screen.tags[i]
      if tag then
        tag:view_only()
      end
    end,
    {description = "view tag #"..i, group = "tag"}
  ),

  -- Toggle tag display
  awful.key({modkey, "Control"}, "#" .. i + 9,
    function ()
      local screen = awful.screen.focused()
      local tag = screen.tags[i]
      if tag then
        awful.tag.viewtoggle(tag)
      end
    end,
  ),

  -- Move client to tag
  awful.key({modkey, "Shift"}, "#" .. i + 9,
    function ()
      if client.focus then
        local tag = client.focus.screen.tags[i]
        if tag then
          client.focus:move_to_tag(tag)
        end
      end
    end,
  ),
)


-- Changing workspaces
keys.clientbuttons = gears.table.join(
  awful.key({modkey,}, "Left",
    function
        awful.tag.viewprev,
    end,
    {description = "view previous tag", group = "tag"}
  ),

  awful.key({modkey,}, "Right",
    function
        awful.tag.viewnext,
    end,
    {description = "view next tag", group = "tag"}
  ),
)
