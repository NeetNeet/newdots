c.tabs.position = "left"
c.tabs.width = "5%"
"""
config.bind(',m', 'spawn mpv {url}')
config.bind(',M', 'hint links spawn mpv {hint-url}')
"""
c.content.cookies.accept = "never"
config.load_autoconfig()
