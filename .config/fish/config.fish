# ~/.config/fish/config.fish

# Alias
alias la='ls -Ah'
alias ll='ls -lAh'
alias clipboard='xclip -selection clipboard'
alias sudo='doas'
alias fucking='doas'
alias xmocon='nvim ~/.xmonad/xmonad.hs'
alias config='/usr/bin/git --git-dir=$HOME/gitshit/dots --work-tree=$HOME'
alias configdots='/usr/bin/git --git-dir=$HOME/gitshit/dots --work-tree=$HOME/gitshit/dots'
alias dots='./home/vivian/scripts/dots_update.sh'
alias gitlisten='/usr/bin/git --git-dir=$HOME/gitshit/dots --work-tree=$HOME update-index --no-assume-unchanged'
alias ls='exa --icons'
alias la='exa -a --icons'

# Greeting
function fish_greeting
	echo Welcome back Vivian
	echo The time is (set_color blue; date +%I:%M%P; set_color normal)
end

starship init fish | source

