" _   ___     _____ __  __    ____             __ _       
"| \ | \ \   / /_ _|  \/  |  / ___|___  _ __  / _(_) __ _ 
"|  \| |\ \ / / | || |\/| | | |   / _ \| '_ \| |_| |/ _` |
"| |\  | \ V /  | || |  | | | |__| (_) | | | |  _| | (_| |
"|_| \_|  \_/  |___|_|  |_|  \____\___/|_| |_|_| |_|\__, |
"                                                   |___/ 



" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'kyazdani42/nvim-web-devicons'
Plug 'ryanoasis/vim-devicons'
Plug 'hugolgst/vimsence'
Plug 'lervag/vimtex'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Vimsense
let g:vimsence_client_id = '439476230543245312'
let g:vimsence_small_text = 'NeoVim'
let g:vimsence_small_image = 'neovim'
let g:vimsence_editing_details = 'Editing: {}'
let g:vimsence_editing_state = 'Working on: {}'
let g:vimsence_file_explorer_text = 'In NERDTree'
let g:vimsence_file_explorer_details = 'Looking for files'
let g:vimsence_custom_icons = {'filetype': 'iconname'}

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }


" Bunch of settings and mappings
set noshowmode
set relativenumber
map <S-n> :NERDTreeToggle<CR>
